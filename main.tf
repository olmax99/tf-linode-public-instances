// --------variables.tf -------
//Global variables
variable "project" {
  type = string
}

variable "slug_version" {
  type = string
}

variable "env" {
  type = string
}

variable "linode_region" {
  type = string
}

variable "linodes" {
  description = "List of Linode ids to which the rule sets will be applied"
  type        = list(string)
  default     = []
}

variable "linode_firewall_config" {
  type        = map(string)
  default     = {}
}

variable "firewall_label" {
  description = "This firewall's human-readable firewall_label"
  type = string
  default = "my-firewall"
}

variable "tags" {
  description = "List of tags to apply to this Firewall"
  type        = list(string)
  default     = []
}

// Secrets
variable "vm_master_pub_key" {
  type      = string
  sensitive = true
}

variable "linode_apiv4_token" {
  type      = string
  sensitive = true
}

// --------versions.tf -------
terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "1.27.1"
    }
  }

  backend "s3" {}

  required_version = ">= 1.1.0"

}

// --------providers.tf ------
provider "linode" {
  token = var.linode_apiv4_token
}

// --------main.tf -----------
resource "linode_instance" "vm" {
        image = "linode/ubuntu18.04"
        label = "Terraform-Web-Example"
        group = "Terraform"
        region = var.linode_region
        type = "g6-standard-1"
        authorized_keys = [ var.vm_master_pub_key ]
}

resource "linode_firewall" "ssh_inbound" {
  label = var.firewall_label
  tags  = var.tags

  inbound {
    protocol = "TCP"
    ports = ["22"]
    addresses = ["0.0.0.0/0"]
  }

  linodes = var.linodes
}
